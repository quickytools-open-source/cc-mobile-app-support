# Crisis Cleanup mobile app support API

Various endpoints for support the Crisis Cleanup mobile apps but most importantly for disabling old builds

The (early access) Android and iOS production builds have disabling switches for preventing app use.

Early access Android prevents use after a specific date

iOS production prevents use below a minimum supported version. Android production will eventually prevent use in a similar manner.

## Steps to disable app builds

### Early access Android cutoff date
Set `earlybirdResponses[key].expires` to a date at which to stop users from using the app.

### iOS production minimum build
Set `minVersionResponses[key].minBuildVersion` to a build number where any build less than will be disabled.

## Testing and deploying
This particular backend uses Firebase Functions. These endpoints are easily replicable to any other backend/API if Firebase isn't workable. Using serverless allows for timely deploys without needing to build and validate an entire API build.
* Configure [`firebase-tools`](https://firebase.google.com/docs/cli) on the local machine.
* Test locally with `npm run serve` from the **`functions`** dir (not the root project dir).
* Deploy with `npm run deploy` from the `functions` dir to a target Firebase project.
* The endpoint URLs and base URL can be seen in the Firebase console > Functions.
* There are test endpoints for local/non-production builds.