/**
 * Import function triggers from their respective submodules:
 *
 * import {onCall} from "firebase-functions/v2/https";
 * import {onDocumentWritten} from "firebase-functions/v2/firestore";
 *
 * See a full list of supported triggers at https://firebase.google.com/docs/functions
 */

import { onRequest } from 'firebase-functions/v2/https'
// import * as logger from "firebase-functions/logger";
import * as express from 'express'
import { Request, Response } from 'express'

// Start writing functions
// https://firebase.google.com/docs/functions/typescript

export const ping = onRequest((_request, response) => {
  // logger.info("Hello logs!", { structuredData: true });
  response.send('pong')
})

const earlybirdResponses = {
  test: {
    expires: new Date(Date.now() - 86.4e6),
    title: 'Testing testing',
    message: ['Red light.', 'Green light.'].join('\n'),
  },
  android: {
    expires: new Date(1693863523605),
    title: 'This app build has expired',
    message: [
      'A new Crisis Cleanup app is available.',
      'Search "Crisis Cleanup" on Google Play.',
      'Or visit the link below.',
      '',
      'Happy volunteering! \uD83D\uDE4C',
    ].join('\n'),
    link: 'https://play.google.com/store/apps/details?id=com.crisiscleanup.prod',
  },
  expired: {
    expires: new Date(Date.now() - 86.4e6),
    title: 'This app build has expired',
    message: [
      'A new Crisis Cleanup app is available.',
      'Search "Crisis Cleanup" on Google Play.',
      'Or visit the link below.',
      '',
      'Happy volunteering! \uD83D\uDE4C',
    ].join('\n'),
    link: 'https://play.google.com/store/apps/details?id=com.crisiscleanup.prod',
  },
}

export const test_earlybird_end = onRequest((_request, response) => {
  response.json(earlybirdResponses.test)
})

export const android_earlybird_end = onRequest((_request, response) => {
  response.json(earlybirdResponses.android)
})

const minVersionResponses = {
  testMin: {
    minBuildVersion: 999,
    title: 'This is a test',
    message: [
      'This is only a test.',
      'No link to see below',
      '',
      'Thank you for tuning in',
    ].join('\n'),
  },
  testAndroidMin: {
    minBuildVersion: 144,
    title: 'This is a test',
    message: [
      'This is only a test.',
      'No link to see below',
      '',
      'Thank you for tuning in',
    ].join('\n'),
  },
  testIosMin: {
    minBuildVersion: 20,
    title: 'This is a test',
    message: [
      'This is only a test.',
      'No link to see below',
      '',
      'Thank you for tuning in',
    ].join('\n'),
  },
  androidMin: {
    // https://play.google.com/console/u/0/developers/8878032622909297799/app/4972352492602220251/tracks/production?tab=releases
    minBuildVersion: 230,
    title: 'This version is no longer supported',
    message: [
      'A new version of Crisis Cleanup is available.',
      'Update this app in Google Play.',
      'Or search "Crisis Cleanup" in Google Play.',
      'Or visit the link below.',
      '',
      'Happy volunteering! \uD83D\uDE4C',
    ].join('\n'),
    link: 'https://play.google.com/store/apps/details?id=com.crisiscleanup.prod',
  },
  iosMin: {
    // https://appstoreconnect.apple.com/teams/235eaf66-0d94-4cad-9951-66257be0dcad/apps/6463570192/testflight/ios
    minBuildVersion: 132,
    title: 'This version is no longer supported',
    message: [
      'A new version of Crisis Cleanup is available.',
      'Update this app in the App Store.',
      'Or search "Crisis Cleanup" on the App Store.',
      'Or visit the link below.',
      '',
      'Happy volunteering! \uD83D\uDE4C',
    ].join('\n'),
    link: 'https://apps.apple.com/us/app/crisis-cleanup/id6463570192',
  },
}

const supportEndpoints = express()

supportEndpoints.get(
  '/min-supported-version/android',
  (_req: Request, res: Response) => {
    return res.json(minVersionResponses.androidMin)
  }
)

supportEndpoints.get(
  '/min-supported-version/ios',
  (_req: Request, res: Response) => {
    return res.json(minVersionResponses.iosMin)
  }
)

supportEndpoints.get(
  '/min-supported-version/test',
  (_req: Request, res: Response) => {
    return res.json(minVersionResponses.testMin)
  }
)

supportEndpoints.get(
  '/min-supported-version/test/android',
  (_req: Request, res: Response) => {
    return res.json(minVersionResponses.testAndroidMin)
  }
)

supportEndpoints.get(
  '/min-supported-version/test/ios',
  (_req: Request, res: Response) => {
    return res.json(minVersionResponses.testIosMin)
  }
)

export const crisis_cleanup_app_support = onRequest(supportEndpoints)
